package com.example.devcreative.midestino.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.devcreative.midestino.R;

public class BaseActivity extends AppCompatActivity {

    private final String TAG = "BASE_ACTV";

    protected ProgressBar mProgressView;

    protected View mEmptyDataView;
    protected View allBoxView;

    protected ImageView mBackImg;

    protected Interpolator mInterpolator;

    protected Handler handler = new Handler();

    protected ProgressDialog pDialog;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    protected void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(200).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    protected void showProgressDialog(int message) {
        try {
            if (pDialog != null)
                pDialog.dismiss();

            pDialog = new ProgressDialog(this);
            pDialog.setMessage(getResources().getString(message));
            pDialog.setCancelable(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        } catch (Throwable e) {
        }
    }

    protected void dismissProgressDialog() {
        try {
            if (pDialog != null) {
                pDialog.dismiss();
            }
        } catch (Throwable e) {
        }
    }

    protected void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    //Messages
    protected void showMessageWithOption(String message, DialogInterface.OnClickListener noListener, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.app_name)).setMessage(message).setNegativeButton(R.string.option_cancel, noListener).setPositiveButton(R.string.option_ok, yesListener).create().show();
    }

    protected void showNoYesMessage(String message, DialogInterface.OnClickListener noListener, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.app_name)).setMessage(message).setNegativeButton(R.string.option_cancel, noListener).setPositiveButton(R.string.option_ok, yesListener).create().show();
    }

    protected void showNoYesMessage(int message, DialogInterface.OnClickListener noListener, DialogInterface.OnClickListener yesListener) {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.app_name)).setMessage(getResources().getString(message)).setNegativeButton(R.string.option_cancel, noListener).setPositiveButton(R.string.option_ok, yesListener).create().show();
    }

    protected void showMessage(String message, DialogInterface.OnClickListener aListener) {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.app_name)).setMessage(message).setPositiveButton(getResources().getString(R.string.option_ok), aListener).create().show();
    }
}
