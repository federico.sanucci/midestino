package com.example.devcreative.midestino.manager;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.example.devcreative.midestino.model.rest.BaseResponse;
import com.example.devcreative.midestino.model.rest.ResponseError;
import com.example.devcreative.midestino.rest.ICallback;
import com.google.gson.Gson;

import okhttp3.ResponseBody;

public class BaseManager {

    private static final String TAG = "BASE_MGR";

    protected Handler handler = new Handler(Looper.getMainLooper());


    protected BaseResponse onGetError(String message) {
        BaseResponse restError = new BaseResponse();
        if (message != null && !message.isEmpty() ) {
            Log.e("ERROR","Error:" + message );
            restError.errorCode = message;
        }
        return restError;
    }

    protected BaseResponse onGetError(ResponseBody errorBody) {
        BaseResponse restError = new BaseResponse();
        if (errorBody != null) {
            Gson gson = new Gson();
            try {
                ResponseError error = gson.fromJson(errorBody.charStream(), ResponseError.class);
                restError.errorCode = error.error.code;
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return restError;
    }

    protected void successRequest(final ICallback callback, final BaseResponse response){
        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onSuccessRequest(response);
            }
        });
    }

    protected void errorRequest(final ICallback callback, final BaseResponse response){
        handler.post(new Runnable() {
            @Override
            public void run() {
                callback.onErrorRequest(response);
            }
        });
    }



    protected BaseResponse onGetError(okhttp3.Response rawResponse){
        BaseResponse restError = new BaseResponse();
        if (rawResponse != null) {
            restError.message = rawResponse.message();
            Log.e("ERROR","Error:" + rawResponse.message() );
        }
        return restError;
    }

    protected BaseResponse onGetThrowableError (Throwable throwable){
        BaseResponse restError = new BaseResponse();
        String errorMsg = "";

        if (throwable.getMessage() != null) {
            String message = throwable.getMessage();
            restError.message = message;
        }
        return restError;
    }

}
