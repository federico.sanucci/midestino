package com.example.devcreative.midestino.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.manager.HotelsManager;
import com.example.devcreative.midestino.model.Hotel;
import com.example.devcreative.midestino.model.rest.BaseResponse;
import com.example.devcreative.midestino.model.services.ResponseHotelInfo;
import com.example.devcreative.midestino.model.services.ResponseHotels;
import com.example.devcreative.midestino.rest.ICallback;
import com.example.devcreative.midestino.rest.ICallbackHotel;
import com.example.devcreative.midestino.utils.Storage;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class LoadScreenActivity extends BaseActivity {

    public final String TAG = "LOADSCREEN_ACT";

    private Context ctx;
    private Handler hTransition = new Handler();
    private Handler hActivity = new Handler();

    private View mLogo;
    private View mLogoNew;
    private int shortAnimationDuration;

    private Button btnDownload;

    private RelativeLayout mProgressContainer;

    private List<Hotel> mHotels;
    private List<Hotel> mTempHotels;

    private ProgressBar mProgressBar;
    private TextView mProgressInfo;
    private int progress;
    private int progressUnity;

    private boolean halfProgressAnimate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_screen);

        Log.e(TAG, "onCreate() -------------");

        ctx = this;

        mLogo = findViewById(R.id.img_logo);
        mLogoNew = findViewById(R.id.img_logo_new);
        mLogoNew.setVisibility(View.GONE);

        btnDownload = (Button) findViewById(R.id.btn_download);
        btnDownload.setVisibility(View.GONE);

        mProgressContainer = (RelativeLayout) findViewById(R.id.progress_container);
        mProgressBar = (ProgressBar) findViewById(R.id.pBar);
        mProgressInfo = (TextView) findViewById(R.id.tView);
        mProgressBar.setProgressTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_red)));

        setupListeners();
        //runAnimation(mLogo, mLogoNew);
        requestDataFromWS();
    }

    private void setupListeners() {
        Log.e(TAG, "setupListeners() -------------");

        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressContainer.setVisibility(View.VISIBLE);
                btnDownload.setVisibility(View.GONE);

                requestDataFromWS();
            }
        });
    }

    private void runAnimation(final View imgBack, final View imgFront) {
        Log.e(TAG, "runAnimation() -------------");

        shortAnimationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        hTransition.postDelayed(new Runnable() {
            @Override
            public void run() {

                imgFront.setAlpha(0f);
                imgFront.setVisibility(View.VISIBLE);
                imgFront.animate()
                        .alpha(1f)
                        .setDuration(shortAnimationDuration)
                        .setListener(null);

                imgBack.animate()
                        .alpha(0f)
                        .setDuration(shortAnimationDuration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                imgBack.setVisibility(View.GONE);
                            }
                        });

                //runAnimation(imgFront, imgBack);
            }
        }, 1000);
    }

    private void requestDataFromWS() {
        Log.e(TAG, "requestDataFromWS() -------------");

        HotelsManager.getInstance().hotelListRequest(ctx, new ICallback() {
            @Override
            public void onSuccessRequest(BaseResponse successResponse) {
                Log.e(TAG, "onSuccess!!!!!");

                ResponseHotels responseList = (ResponseHotels) successResponse;
                if (responseList.response == null || responseList.response.isEmpty()){

                    showMessage(successResponse.message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            mProgressContainer.setVisibility(View.GONE);
                            btnDownload.setVisibility(View.VISIBLE);
                        }
                    });
                } else {

                    mHotels = responseList.response;
                    Storage.getInstance().setDataList(mHotels);
                    toDownloadContent();
                }
            }

            @Override
            public void onErrorRequest(BaseResponse errorResponse) {
                Log.e(TAG, "onErrorRequest!!!!");

                showMessage(errorResponse.message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mProgressContainer.setVisibility(View.GONE);
                        btnDownload.setVisibility(View.VISIBLE);
                    }
                });
            }
        });
    }

    private void toDownloadContent() {
        Log.e(TAG, "toDownloadContent() -------------");

        progressUnity = 100 / mHotels.size();
        progress = mProgressBar.getProgress();
        mProgressInfo.setText(progress + "/" + mProgressBar.getMax());
        mTempHotels = new ArrayList<>();

        runBackgroundTasks(mHotels, new ICallback() {
            @Override
            public void onSuccessRequest(BaseResponse successResponse) {
                Log.e(TAG, "Background Tasks Complete!!!");

                Storage.getInstance().setDataList(mTempHotels);
                toMain();
            }

            @Override
            public void onErrorRequest(BaseResponse errorResponse) {
                Log.e(TAG, "Background Tasks Fail!!!");

                mProgressContainer.setVisibility(View.GONE);
                btnDownload.setVisibility(View.VISIBLE);
            }
        });

    }

    private void runBackgroundTasks(final List<Hotel> hotels, final ICallback callback) {
        Log.e(TAG, "runBackgroundTasks() -------------");

        if (!hotels.isEmpty()) {

            progress = progress + progressUnity;

            String progressDetail = progress + "/" + mProgressBar.getMax();
            if (mProgressBar.getProgress() <= 33) {

                progressDetail = progressDetail + "   " + getString(R.string.load_activity_data_download);

            } else if(mProgressBar.getProgress() >= 50 && halfProgressAnimate) {

                halfProgressAnimate = false;
                progressDetail = progressDetail + "   " + getString(R.string.load_activity_data_download);
                runAnimation(mLogo, mLogoNew);

            } else if (mProgressBar.getProgress() <= 90) {

                progressDetail = progressDetail + "   " + getString(R.string.load_activity_data_download2);
            } else {

                progressDetail = progressDetail + "   " + getString(R.string.load_activity_data_download3);
            }

            mProgressInfo.setText(progressDetail);
            mProgressBar.setProgress(progress);

            Hotel hotel = hotels.remove(0);
            Log.e(TAG, "Hotel ID: " + hotel.id);

            ImageView downloadImage = new ImageView(ctx);
            new DownloadImageFromInternet(hotel, callback).execute(hotel.main_picture);

            //runBackgroundTasks(hotels, callback);

        } else {
            progress = 100;
            String progressDetail = progress + "/" + mProgressBar.getMax() + "   " + getString(R.string.load_activity_data_download3);
            mProgressInfo.setText(progressDetail);
            mProgressBar.setProgress(progress);

            callback.onSuccessRequest(new BaseResponse());
        }
    }

    private void toMain(){
        Log.e(TAG, "toMain() -------------");

        hActivity.postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(LoadScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }

    private void downloadHotelContent(final Hotel hotel, final ICallback callback) {
        Log.e(TAG, "downloadHotelContent() -------------");

        HotelsManager.getInstance().getHotelContent(hotel, ctx, new ICallbackHotel() {
            @Override
            public void onSuccessRequest(BaseResponse successResponse) {

                ResponseHotelInfo response = (ResponseHotelInfo) successResponse;
                mTempHotels.add(response.response);
                runBackgroundTasks(mHotels, callback);
            }

            @Override
            public void onErrorRequest(BaseResponse errorResponse) {
                Log.e(TAG, "HotelContent -> Error: " + errorResponse.message);

                mTempHotels.add(hotel);
                runBackgroundTasks(mHotels, callback);
            }
        });
    }

    private class DownloadImageFromInternet extends AsyncTask<String, Void, Bitmap> {
        Hotel hotel;
        ICallback callback;

        public DownloadImageFromInternet(Hotel hotel, ICallback callback) {
            this.hotel = hotel;
            this.callback = callback;
        }

        protected Bitmap doInBackground(String... urls) {
            String imageURL = urls[0];
            Bitmap bimage = null;
            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                bimage = BitmapFactory.decodeStream(in);

            } catch (Exception e) {
                Log.e("Error Message", e.getMessage());
                e.printStackTrace();
            }
            return bimage;
        }

        protected void onPostExecute(Bitmap result) {
            Log.e(TAG, "FINISH!!!");
            hotel.logo = result;

            downloadHotelContent(hotel, callback);
        }
    }
}
