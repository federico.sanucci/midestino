package com.example.devcreative.midestino.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.adapter.HotelItemRecyclerViewAdapter;
import com.example.devcreative.midestino.model.Hotel;
import com.example.devcreative.midestino.rest.ICallbackHotelClick;
import com.example.devcreative.midestino.utils.Storage;

import java.util.List;

public class MainActivity extends BaseActivity {

    public final String TAG = "MAIN_ACT";

    private HotelItemRecyclerViewAdapter mAdapter;
    private Context ctx;
    private List<Hotel> mHotels;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e(TAG, "onCreate() -------------");

        ctx = this;
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(ctx));

        mHotels = Storage.getInstance().getDataList();
        if (mHotels == null || mHotels.isEmpty()){
            Log.e(TAG, "Lista vacía!!!!!!");
        } else {

            setupListeners();
            mAdapter = new HotelItemRecyclerViewAdapter(mHotels, ctx);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private void setupListeners() {
        Log.e(TAG, "setupListeners() -------------");

        for (Hotel hotel : mHotels) {
            hotel.hotelClick = new ICallbackHotelClick() {
                @Override
                public void onClickItem(Hotel hotel, int position) {
                    Log.e(TAG, "onClickItem -> Hotel ID: " + hotel.id + " | Position: " + position);
                    hotel.showDescription = !hotel.showDescription;
                    mAdapter.notifyItemChanged(position);
                }

                @Override
                public void onClickDetail(Hotel hotel, int position) {
                    Log.e(TAG, "onClickDetail -> Hotel ID: " + hotel.id + " | Position: " + position);

                    Storage.getInstance().setHotelDisplay(hotel);
                    Intent intent = new Intent(MainActivity.this, HotelDetailActivity.class);
                    startActivity(intent);
                }

                @Override
                public void onClickLocation(Hotel hotel, int position) {
                    Log.e(TAG, "onClickLocation -> Hotel ID: " + hotel.id + " | Position: " + position);

                    Storage.getInstance().setHotelDisplay(hotel);
                    Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                    startActivity(intent);
                }
            };
        }
    }
}
