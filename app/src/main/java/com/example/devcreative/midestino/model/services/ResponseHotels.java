package com.example.devcreative.midestino.model.services;

import com.example.devcreative.midestino.model.Hotel;
import com.example.devcreative.midestino.model.rest.BaseResponse;

import java.util.List;

public class ResponseHotels extends BaseResponse {

    public List<Hotel> response = null;
}
