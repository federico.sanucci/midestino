package com.example.devcreative.midestino.model.rest;

public class BaseResponse {

    public boolean result = false;
    public String errorCode = "";
    public String message = "";
}
