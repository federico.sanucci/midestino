package com.example.devcreative.midestino.rest;

public class RestConstants {

    public static final String BASE_END_POINT = "http://private-a2ba2-jovenesdealtovuelo.apiary-mock.com/";

    public static final long READ_TIME_OUT =  300L;
    public static final long CNX_TIME_OUT =  300L;
}
