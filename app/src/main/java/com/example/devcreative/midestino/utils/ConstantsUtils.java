package com.example.devcreative.midestino.utils;

public class ConstantsUtils {

    //WS Constants
    public final static String SERVICE_CONTENT_KEY = "Content-Type";
    public final static String SERVICE_CONTENT_VALUE = "application/json";
    public final static String SERIVCE_ACCEPT_KEY = "Accept";
    public final static String SERIVCE_ACCEPT_VALUE = "application/json";

    public final static String SERVICE_ERROR_EX = "5000";
    public final static String SERVICE_ERROR_EMPTY_NULL_DATA = "5001";

}
