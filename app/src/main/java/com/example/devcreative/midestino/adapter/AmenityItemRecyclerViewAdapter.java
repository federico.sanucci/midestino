package com.example.devcreative.midestino.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.model.Amenity;

import java.util.List;

public class AmenityItemRecyclerViewAdapter extends RecyclerView.Adapter<AmenityItemRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Amenity> mValues;

    public AmenityItemRecyclerViewAdapter(List<Amenity> items, Context newContext) {
        mValues = items;
        mContext = newContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_amenity_item, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        if (holder.mItem.description == null || holder.mItem.description.isEmpty()) {
            holder.mMainContainer.setVisibility(View.GONE);
        } else {
            holder.mMainContainer.setVisibility(View.VISIBLE);
            holder.mAmenity.setVisibility(View.VISIBLE);
            holder.mAmenity.setText(holder.mItem.description);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public Amenity mItem;
        public TextView mAmenity;
        public LinearLayout mMainContainer;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            mAmenity = (TextView) view.findViewById(R.id.amenity_desc);
            mMainContainer = (LinearLayout) view.findViewById(R.id.item_container);
        }
    }
}