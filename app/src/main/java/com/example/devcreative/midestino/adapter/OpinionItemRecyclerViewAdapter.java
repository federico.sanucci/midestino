package com.example.devcreative.midestino.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.model.Review;

import java.util.List;

public class OpinionItemRecyclerViewAdapter extends RecyclerView.Adapter<OpinionItemRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Review> mValues;

    public OpinionItemRecyclerViewAdapter(List<Review> items, Context newContext) {
        mValues = items;
        mContext = newContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_opinion_item, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        if (holder.mItem.user == null || holder.mItem.user.name == null || holder.mItem.user.name.isEmpty()) {
            holder.mMainContainer.setVisibility(View.GONE);
        } else {
            holder.mMainContainer.setVisibility(View.VISIBLE);

            String userName = holder.mItem.user.name;
            if (holder.mItem.user.country != null && !holder.mItem.user.country.isEmpty()){
                userName = userName + " (" + holder.mItem.user.country + ")";
            }
            holder.mUser.setText(userName);

            if (holder.mItem.comment == null) {
                holder.mMainContainer.setVisibility(View.GONE);
            } else {
                holder.mMainContainer.setVisibility(View.VISIBLE);

                if (holder.mItem.comment.good == null || holder.mItem.comment.good.isEmpty()){
                    holder.mPossitiveOpinionContainer.setVisibility(View.GONE);
                } else {
                    holder.mPossitiveOpinionContainer.setVisibility(View.VISIBLE);

                    holder.mPossitiveOpinion.setText(holder.mItem.comment.good);
                }

                if (holder.mItem.comment.bad == null || holder.mItem.comment.bad.isEmpty()){
                    holder.mNegativeOpinionContainer.setVisibility(View.GONE);
                } else {
                    holder.mNegativeOpinionContainer.setVisibility(View.VISIBLE);

                    holder.mNegativeOpinion.setText(holder.mItem.comment.bad);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public Review mItem;

        public TextView mUser;
        public TextView mPossitiveOpinion;
        public TextView mNegativeOpinion;
        public LinearLayout mPossitiveOpinionContainer;
        public LinearLayout mNegativeOpinionContainer;
        public LinearLayout mMainContainer;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            mUser = (TextView) view.findViewById(R.id.user);
            mMainContainer = (LinearLayout) view.findViewById(R.id.item_container);

            mPossitiveOpinionContainer = (LinearLayout) view.findViewById(R.id.positive_opinion_container);
            mNegativeOpinionContainer = (LinearLayout) view.findViewById(R.id.negative_opinion_container);

            mPossitiveOpinion = (TextView) view.findViewById(R.id.positive_opinion_desc);
            mNegativeOpinion = (TextView) view.findViewById(R.id.negative_opinion_desc);
        }
    }
}