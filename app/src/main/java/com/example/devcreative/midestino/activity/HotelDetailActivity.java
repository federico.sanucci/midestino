package com.example.devcreative.midestino.activity;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.adapter.AmenityItemRecyclerViewAdapter;
import com.example.devcreative.midestino.adapter.OpinionItemRecyclerViewAdapter;
import com.example.devcreative.midestino.model.Hotel;
import com.example.devcreative.midestino.utils.Storage;

public class HotelDetailActivity extends BaseActivity {

    public final String TAG = "DETAIL_ACT";

    private Context ctx;
    private Hotel hotel;

    private TextView mName;
    private TextView mAddress;
    private TextView mDescription;
    private ImageView mIcon;
    private TextView mAmenities;
    private TextView mOpinions;

    private AmenityItemRecyclerViewAdapter mAmenityAdapter;
    private RecyclerView mRecylcerViewAmenities;
    private OpinionItemRecyclerViewAdapter mOpinionAdapter;
    private RecyclerView mRecyclerViewOpinion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);

        Log.e(TAG, "onCreate() -------------");

        ctx = this;

        hotel = Storage.getInstance().getHotelDisplay();
        HotelDetailActivity.this.setTitle(hotel.name);

        mName = (TextView) findViewById(R.id.name);
        mAddress = (TextView) findViewById(R.id.address);
        mDescription = (TextView) findViewById(R.id.description);
        mIcon = (ImageView) findViewById(R.id.icon);

        mRecylcerViewAmenities = (RecyclerView) findViewById(R.id.recycler_view_amenities);
        mRecylcerViewAmenities.setLayoutManager(new LinearLayoutManager(ctx));
        mRecyclerViewOpinion = (RecyclerView) findViewById(R.id.recycler_view_opinions);
        mRecyclerViewOpinion.setLayoutManager(new LinearLayoutManager(ctx));


        Typeface fontTitle = Typeface.createFromAsset(ctx.getAssets(), "dream_orphans.ttf");
        mName.setTypeface(fontTitle);

        mAmenities = (TextView) findViewById(R.id.amenities);
        mOpinions = (TextView) findViewById(R.id.opinions);
        Typeface fontSubtitle = Typeface.createFromAsset(ctx.getAssets(), "caviar_dreams.ttf");
        mAmenities.setTypeface(fontSubtitle);
        mOpinions.setTypeface(fontSubtitle);

        initActivity();
    }

    private void initActivity() {
        Log.e(TAG, "initActivity() -------------");

        mName.setText(hotel.name);
        mAddress.setText(hotel.address);
        mDescription.setText(hotel.description);
        mIcon.setImageBitmap(hotel.logo);

        if (hotel.amenities != null && !hotel.amenities.isEmpty()) {
            mAmenityAdapter = new AmenityItemRecyclerViewAdapter(hotel.amenities, ctx);
            mRecylcerViewAmenities.setAdapter(mAmenityAdapter);
        } else {
            mRecylcerViewAmenities.setVisibility(View.GONE);
            mAmenities.setVisibility(View.GONE);
        }

        if (hotel.reviews != null && !hotel.reviews.isEmpty()) {
            mOpinionAdapter = new OpinionItemRecyclerViewAdapter(hotel.reviews, ctx);
            mRecyclerViewOpinion.setAdapter(mOpinionAdapter);
        } else {
            mRecyclerViewOpinion.setVisibility(View.GONE);
            mOpinions.setVisibility(View.GONE);
        }
    }
}
