package com.example.devcreative.midestino.rest;

import com.example.devcreative.midestino.model.rest.BaseResponse;

public interface ICallback {

    public void onSuccessRequest(BaseResponse successResponse);
    public void onErrorRequest(BaseResponse errorResponse);
}
