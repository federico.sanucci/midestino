package com.example.devcreative.midestino.model.rest;

public class Error {

    public String id = "";
    public String code = "";
    public String message = "";
}
