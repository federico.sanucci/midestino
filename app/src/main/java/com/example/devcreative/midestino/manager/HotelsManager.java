package com.example.devcreative.midestino.manager;

import android.content.Context;
import android.util.Log;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.model.Amenity;
import com.example.devcreative.midestino.model.Comment;
import com.example.devcreative.midestino.model.Currency;
import com.example.devcreative.midestino.model.GeoLocation;
import com.example.devcreative.midestino.model.Hotel;
import com.example.devcreative.midestino.model.Price;
import com.example.devcreative.midestino.model.Review;
import com.example.devcreative.midestino.model.User;
import com.example.devcreative.midestino.model.services.ResponseHotelInfo;
import com.example.devcreative.midestino.model.services.ResponseHotels;
import com.example.devcreative.midestino.rest.ICallback;
import com.example.devcreative.midestino.rest.ICallbackHotel;
import com.example.devcreative.midestino.rest.IRest;
import com.example.devcreative.midestino.rest.RestUtils;
import com.example.devcreative.midestino.utils.ConstantsUtils;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HotelsManager extends BaseManager {

    private static final String TAG = "HOTELS_MGR";

    private static HotelsManager instance = new HotelsManager();

    public static synchronized HotelsManager getInstance() {
        return instance;
    }

    public void hotelListRequest(final Context ctx, final ICallback callback) {
        Log.e(TAG, "hotelListRequest() -------------");

        RestUtils restUtils = RestUtils.getInstance();
        final IRest iRest = restUtils.createService(IRest.class);

        final Map<String, String> headerMap = new HashMap<>();
        headerMap.put(ConstantsUtils.SERVICE_CONTENT_KEY, ConstantsUtils.SERVICE_CONTENT_VALUE);
        headerMap.put(ConstantsUtils.SERIVCE_ACCEPT_KEY, ConstantsUtils.SERIVCE_ACCEPT_VALUE);

        Call hotelsCall = iRest.hotelsRequest(headerMap);
        hotelsCall.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                ResponseHotels responseHotels = new ResponseHotels();
                if (response.isSuccessful()) {

                    Log.e(TAG, "response: " + response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        JSONArray jHotelsArray = jsonObject.optJSONArray("items");
                        if (jHotelsArray != null && jHotelsArray.length() > 0){

                            List<Hotel> hotelList = new ArrayList<>();

                            for (int k = 0; k < jHotelsArray.length(); k++) {
                                JSONObject item = jHotelsArray.optJSONObject(k);

                                Hotel hotel = new Hotel();
                                hotel.id = item.optInt("id");
                                hotel.stars = item.optInt("stars");
                                hotel.name = item.optString("name");
                                hotel.address = item.optString("address");
                                hotel.main_picture = item.optString("main_picture");
                                hotel.rating = item.optDouble("rating");

                                JSONArray jAmenitiesArray = item.optJSONArray("amenities");
                                if (jAmenitiesArray != null && jAmenitiesArray.length() > 0) {
                                    List<Amenity> amenities = new ArrayList<>();

                                    for (int j = 0; j < jAmenitiesArray.length(); j++){
                                        JSONObject iAmenity = jAmenitiesArray.optJSONObject(j);

                                        Amenity amenity = new Amenity();
                                        amenity.id = iAmenity.optString("id");
                                        amenity.description = iAmenity.optString("description");
                                        amenities.add(amenity);
                                    }

                                    hotel.amenities = amenities;
                                }

                                JSONObject priceObj = item.optJSONObject("price");
                                if (priceObj != null){
                                    Price price = new Price();

                                    JSONObject currencyObj = priceObj.optJSONObject("currency");
                                    if (currencyObj != null){

                                        Currency currency = new Currency();
                                        currency.code = currencyObj.optString("code");
                                        currency.mask = currencyObj.optString("mask");
                                        currency.ratio = currencyObj.optDouble("ratio");
                                        price.currency = currency;
                                    }

                                    price.final_price = priceObj.optBoolean("final_price");
                                    price.base = priceObj.optInt("base");
                                    price.best = priceObj.optInt("best");

                                    hotel.price = price;
                                }

                                hotelList.add(hotel);

                            }

                            responseHotels.response = hotelList;

                            callback.onSuccessRequest(responseHotels);

                        } else {
                            responseHotels.result = false;
                            responseHotels.errorCode = ConstantsUtils.SERVICE_ERROR_EMPTY_NULL_DATA;
                            responseHotels.message = ctx.getString(R.string.error_5001);
                            callback.onSuccessRequest(responseHotels);
                        }

                    } catch (Exception e) {
                        Log.e(TAG, "Exception: " + e.getMessage());
                        responseHotels.result = false;
                        responseHotels.errorCode = ConstantsUtils.SERVICE_ERROR_EX;
                        responseHotels.message = e.getMessage();
                        callback.onErrorRequest(responseHotels);
                    }

                } else {
                    responseHotels.result = false;
                    responseHotels.errorCode = String.valueOf(response.code());
                    responseHotels.message = response.message();

                    callback.onErrorRequest(responseHotels);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

                if (call.isCanceled()) {
                    Log.e(TAG, "User cancell operation....");
                }

                callback.onErrorRequest(onGetThrowableError(t));
            }
        });
    }

    public void getHotelContent(final Hotel hotel, final Context ctx, final ICallbackHotel callback) {
        Log.e(TAG, "getHotelContent() -------------");

        RestUtils restUtils = RestUtils.getInstance();
        final IRest iRest = restUtils.createService(IRest.class);

        final Map<String, String> headerMap = new HashMap<>();
        headerMap.put(ConstantsUtils.SERVICE_CONTENT_KEY, ConstantsUtils.SERVICE_CONTENT_VALUE);
        headerMap.put(ConstantsUtils.SERIVCE_ACCEPT_KEY, ConstantsUtils.SERIVCE_ACCEPT_VALUE);

        Call hotelsCall = iRest.hotelInfoRequest(headerMap, hotel.id);
        hotelsCall.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                Log.e(TAG, "onResponse");

                ResponseHotelInfo responseHotelInfo = new ResponseHotelInfo();
                if (response.isSuccessful()) {

                    Hotel data = hotel;

                    Log.e(TAG, "response: " + response.body());
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().toString());
                        JSONObject hotelData = jsonObject.optJSONObject("hotel");

                        if (hotelData != null) {

                            data.description = hotelData.getString("description");

                            JSONObject locationJson = hotelData.optJSONObject("geo_location");
                            if (locationJson != null) {

                                GeoLocation geoLocation = new GeoLocation();
                                geoLocation.latitude = locationJson.optDouble("latitude");
                                geoLocation.longitude = locationJson.optDouble("longitude");
                                data.geoLocation = geoLocation;
                            }

                            List<Review> reviews = new ArrayList<>();
                            JSONArray jReviewsArray = hotelData.optJSONArray("reviews");
                            if (jReviewsArray != null && jReviewsArray.length() > 0) {

                                for (int j = 0; j < jReviewsArray.length(); j++) {
                                    JSONObject iReview = jReviewsArray.optJSONObject(j);

                                    Review review = new Review();

                                    JSONObject jComment = iReview.optJSONObject("comments");
                                    if (jComment != null) {

                                        Comment comment = new Comment();
                                        comment.good = jComment.optString("good");
                                        comment.bad = jComment.optString("bad");
                                        comment.type = jComment.optString("type");
                                        review.comment = comment;
                                    }

                                    JSONObject jUser = iReview.optJSONObject("user");
                                    if (jUser != null) {

                                        User user = new User();
                                        user.name = jUser.optString("name");
                                        user.first_name = jUser.optString("first_name");
                                        user.last_name = jUser.optString("last_name");
                                        user.country = jUser.optString("country");
                                        review.user = user;
                                    }

                                    reviews.add(review);
                                }
                            }

                            data.reviews.addAll(reviews);

                        }

                        responseHotelInfo.response = data;
                        callback.onSuccessRequest(responseHotelInfo);

                    } catch (Exception e) {
                        Log.e(TAG, "Exception: " + e.getMessage());
                        responseHotelInfo.result = false;
                        responseHotelInfo.errorCode = ConstantsUtils.SERVICE_ERROR_EX;
                        responseHotelInfo.message = e.getMessage();
                        callback.onErrorRequest(responseHotelInfo);
                    }

                } else {
                    responseHotelInfo.result = false;
                    responseHotelInfo.errorCode = String.valueOf(response.code());
                    responseHotelInfo.message = response.message();

                    callback.onErrorRequest(responseHotelInfo);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e(TAG, "onFailure");

                if (call.isCanceled()) {
                    Log.e(TAG, "User cancell operation....");
                }

                callback.onErrorRequest(onGetThrowableError(t));
            }
        });
    }
}
