package com.example.devcreative.midestino.rest;

import com.example.devcreative.midestino.model.Hotel;
import com.example.devcreative.midestino.model.rest.BaseResponse;

public interface ICallbackHotel {

    public void onSuccessRequest(BaseResponse successResponse);
    public void onErrorRequest(BaseResponse errorResponse);
}
