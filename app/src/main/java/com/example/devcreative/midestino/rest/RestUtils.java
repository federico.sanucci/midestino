package com.example.devcreative.midestino.rest;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.functions.Action1;

public class RestUtils {

    public static final String TAG = "REST";

    private static RestUtils instance;
    private OkHttpClient httpClient;
    private Retrofit.Builder builder;

    public static synchronized RestUtils getInstance() {
        if (instance == null) {
            instance = new RestUtils();
        }
        return instance;
    }

    private RestUtils() {

        httpClient = new OkHttpClient.Builder().readTimeout(RestConstants.READ_TIME_OUT, TimeUnit.SECONDS).connectTimeout(RestConstants.CNX_TIME_OUT, TimeUnit.SECONDS).build();
        // temp_default endPoint

        builder = new Retrofit.Builder().baseUrl(RestConstants.BASE_END_POINT).addConverterFactory(GsonConverterFactory.create());
    }


    public void setEndPoint(String endPoint){
        //builder = new Retrofit.Builder().baseUrl(RestConstants.BASE_END_POINT).addConverterFactory(GsonConverterFactory.create());
        //builder = new Retrofit.Builder().baseUrl(endPoint);
    }


    public  <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }

    private Action1<Throwable> mOnError = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            Log.e(TAG, throwable.getMessage());
            throwable.printStackTrace();
        }
    };
}
