package com.example.devcreative.midestino.utils;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class AppController extends Application {

    private static final String TAG = "APP";
    private static AppController mInstance;

    public static long FREE_MEORY_DELAY = 10000L;

    private Context ctx;
    private Handler handler = new Handler();

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Log.e(TAG, "ApplicationController onCreate...........................");
        ctx = this;

        Storage.getInstance().init(this.getApplicationContext());

        freeMemory();
    }

    private void freeMemory() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                Log.e(TAG, "freeMemory ????????????????????????????????????????????????????????");
                Runtime.getRuntime().gc();

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        freeMemory();
                    }
                }, FREE_MEORY_DELAY);

            }
        }).start();
    }
}
