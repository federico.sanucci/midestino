package com.example.devcreative.midestino.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.devcreative.midestino.model.GeoLocation;
import com.example.devcreative.midestino.model.Hotel;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private static final String TAG = "Storage";
    private static final String SHARED_PREFERENCES = "shared_pref";

    private SharedPreferences sharedPreferences;
    private static Storage instance;

    private Context appContext = null;

    private List<Hotel> dataList = new ArrayList<Hotel>();
    private Hotel hotelDisplay = null;

    public static synchronized Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }

    public void init(Context ctx) {

        if (sharedPreferences == null) {
            sharedPreferences = ctx.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        }
    }

    public String getPreferences(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void putPreferences(String key, String value) {
        if (key != null && key.trim().length() > 0 && value != null && value.trim().length() > 0) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
        }
    }

    public long getLongPreferences(String key) {
        return sharedPreferences.getLong(key,0);
    }

    public void putLongPreferences(String key, long value) {
        if (key != null && key.trim().length() > 0) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putLong(key,value);
            editor.commit();
        }
    }

    public boolean getBoolPreferences(String key) {
        return sharedPreferences.getBoolean(key,true);
    }

    public void putBoolPreferences(String key, boolean value) {
        if (key != null && key.trim().length() > 0) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(key,value);
            editor.commit();
        }
    }

    public void remove(String key){
        if (key != null && key.trim().length() > 0) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(key);
            editor.commit();
        }
    }

    public List<Hotel> getDataList() {
        return dataList;
    }

    public void setDataList(List<Hotel> dataList) {
        this.dataList = dataList;
    }

    public Hotel getHotelDisplay() {
        return hotelDisplay;
    }

    public void setHotelDisplay(Hotel hotelDisplay) {
        this.hotelDisplay = hotelDisplay;
    }
}
