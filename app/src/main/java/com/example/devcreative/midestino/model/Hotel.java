package com.example.devcreative.midestino.model;

import android.graphics.Bitmap;

import com.example.devcreative.midestino.rest.ICallbackHotelClick;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    public int id = -1;
    public int stars = -1;
    public String name = "";
    public String address = "";
    public String main_picture = "";
    public double rating = 0;

    public List<Amenity> amenities = new ArrayList<>();
    public Price price = null;

    public Bitmap logo = null;
    public String description = "";
    public GeoLocation geoLocation = null;
    public List<Review> reviews = new ArrayList<>();
    public boolean showDescription = false;

    public ICallbackHotelClick hotelClick;

}
