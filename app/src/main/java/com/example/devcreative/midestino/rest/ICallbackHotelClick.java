package com.example.devcreative.midestino.rest;

import com.example.devcreative.midestino.model.Hotel;

public interface ICallbackHotelClick {

    public void onClickItem(Hotel hotel, int position);
    public void onClickDetail(Hotel hotel, int position);
    public void onClickLocation(Hotel hotel, int position);
}
