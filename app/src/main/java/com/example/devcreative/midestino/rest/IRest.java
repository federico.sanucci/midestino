package com.example.devcreative.midestino.rest;

import com.google.gson.JsonElement;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IRest {

/* EJ:
    @POST("Login")
    public Call<JsonElement> loginRequest(@HeaderMap Map<String, String> headers, @Body RequestBody body);

    @GET("GetConfiguration")
    public Call<JsonElement> getConfigurationRequest(@HeaderMap Map<String, String> headers, @Query("AuthToken") String authToken, @Query("DeviceId") String deviceId);
 */

    @GET("hotels")
    public Call<JsonElement> hotelsRequest(@HeaderMap Map<String, String> headers);

    @GET("hotels/{id}")
    public Call<JsonElement> hotelInfoRequest(@HeaderMap Map<String, String> headers, @Path("id") int hotelId);
}
