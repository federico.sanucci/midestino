package com.example.devcreative.midestino.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.devcreative.midestino.R;
import com.example.devcreative.midestino.model.Hotel;

import java.util.List;

import static android.view.View.GONE;

public class HotelItemRecyclerViewAdapter extends RecyclerView.Adapter<HotelItemRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Hotel> mValues;

    public HotelItemRecyclerViewAdapter(List<Hotel> items, Context newContext) {
        mValues = items;
        mContext = newContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.spinner_item, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);

        if (holder.mItem.name == null || holder.mItem.name.isEmpty()) {
            holder.mItemContainer.setVisibility(GONE);
            holder.mMainContainer.setVisibility(GONE);
            holder.mDescContainer.setVisibility(GONE);
        } else {
            holder.mItemContainer.setVisibility(View.VISIBLE);
            holder.mName.setVisibility(View.VISIBLE);
            holder.mName.setText(holder.mItem.name);

            Typeface font = Typeface.createFromAsset(mContext.getAssets(), "dream_orphans.ttf");
            holder.mName.setTypeface(font);
        }

        if (holder.mItem.stars == -1) {
            holder.mRate.setVisibility(GONE);
        } else {
            holder.mRate.setVisibility(View.VISIBLE);
            //holder.mRate.setNumStars(holder.mItem.stars);
            holder.mRate.setRating(holder.mItem.stars);
            holder.mRate.setClickable(false);

            //LayerDrawable starColor = (LayerDrawable) holder.mRate.getProgressDrawable();
            //starColor.getDrawable(2).setColorFilter(mContext.getColor(R.color.color_yellow), PorterDuff.Mode.SRC_ATOP);
        }

        if (holder.mItem.logo == null) {
            holder.mLogo.setVisibility(GONE);
        } else {
            holder.mLogo.setVisibility(View.VISIBLE);
            holder.mLogo.setImageBitmap(holder.mItem.logo);
        }

        if (holder.mItem.description == null || holder.mItem.description.isEmpty()) {
            holder.mDescContainer.setVisibility(GONE);
        } else {
            holder.mDescContainer.setVisibility(View.VISIBLE);
            holder.mDescription.setText(holder.mItem.description);

            if (holder.mItem.showDescription) {
                holder.mDescContainer.setVisibility(View.VISIBLE);
            } else {
                holder.mDescContainer.setVisibility(GONE);
            }
        }

        holder.mItemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mItem.hotelClick.onClickItem(holder.mItem, position);
            }
        });

        holder.mDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mItem.hotelClick.onClickDetail(holder.mItem, position);
            }
        });

        holder.mLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mItem.hotelClick.onClickLocation(holder.mItem, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        public ImageView mLogo;
        public TextView mName;
        public RatingBar mRate;
        public Button mDetails;
        public Button mLocation;
        public TextView mDescription;

        public LinearLayout mItemContainer;
        public LinearLayout mMainContainer;
        public ViewGroup mDescContainer;

        public Hotel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;

            mLogo = (ImageView) view.findViewById(R.id.img_hotel);
            mName = (TextView) view.findViewById(R.id.txt_name);
            mRate = (RatingBar) view.findViewById(R.id.rating_bar);
            mDetails = (Button) view.findViewById(R.id.btn_details);
            mLocation = (Button) view.findViewById(R.id.btn_location);
            mDescription = (TextView) view.findViewById(R.id.txt_description);

            mItemContainer = (LinearLayout) view.findViewById(R.id.item_container);
            mMainContainer = (LinearLayout) view.findViewById(R.id.main_container);
            mDescContainer = (ViewGroup) view.findViewById(R.id.description_container);
        }
    }
}
